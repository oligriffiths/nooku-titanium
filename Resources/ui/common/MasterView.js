//Master View Component Constructor
function MasterView(parent, win) {
	//create object instance, parasitic subclass of Observable
	var self = Ti.UI.createView({
		backgroundColor:'white'
	});
	
	//some dummy data for our table view
	var tableData = [];
	
	var table = Ti.UI.createTableView();
	self.add(table);
	
	//add behavior
	table.addEventListener('click', function(e) {
		self.fireEvent('itemSelected', {
			id: e.rowData.id,
			title: e.rowData.title,
			desc: e.rowData.desc,
			data: e.rowData.data		
		});
	});

	table.addEventListener('delete', function(e) {
		win.makeRequest('http://nooku-clean.loc/?option=com_articles&view=article&id='+e.rowData.id,'DELETE', function(response, status){
			if(status == 204){
				alert('Resource deleted')
			}else{
				alert('Unable to delete resource')
			}
		})
	});

	
	var editButton = Titanium.UI.createButton({
		title: 'Edit',
		systemButton: Ti.UI.iPhone.SystemButton.PLAIN
	});
	
	editButton.addEventListener('click',function(e)
	{
   		if (e.source.title == "Edit") {
	        editButton.title = "Done";
	        table.setEditing(true);	        
	    } else {
	        editButton.title = "Edit";
	        table.setEditing(false);	        
	    }  
	});
	parent.setLeftNavButton(editButton);
	
	var addButton = Titanium.UI.createButton({
		systemButton: Ti.UI.iPhone.SystemButton.ADD
	});
	
	var counter = 0;
	addButton.addEventListener('click',function(e)
	{
		counter++;
		var data = {
   			title: 'Titanium '+ counter,
   			published: 1,
   			description: 'added through titanium'
   		}
   		
   		win.makeRequest('http://nooku-clean.loc/?option=com_articles', 'POST', function(response, status, e){
   			
   			if(status == 201)
   			{
   				data.desc = data.description
   				table.appendRow(data);
   			}else{
   				alert('Resource was unable to be created')
   			}
	         
	         
   		}, data)
	});
		
	parent.setRightNavButton(addButton);
	
	win.makeRequest = function (url, method, callback, data)
	{
		 var client = Ti.Network.createHTTPClient({
		 	
		    // function called when the response data is available
		    onload : function(e) 
		    {
		         callback(JSON.parse(this.responseText), this.status, e);	  
		    },
		    // function called when an error occurs, including a timeout
		    onerror : function(e) 
		    {
		         Ti.API.debug(e.error);
		         alert(e.error);
		    },
		    timeout : 5000  // in milliseconds
		 });
		 // Prepare the connection.
		 client.open(method, url+'&format=json');
		 
		 //Set JSON header
		 client.setRequestHeader('Accpet','application/json');
	 	 if(method.toLowerCase() == 'get') client.setRequestHeader('Content-Type','application/json');
		 else client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		 
		 var username = 'admin@localhost.home'
		 var password = 'admin'
		 
		 client.setRequestHeader('Authorization', 'Basic ' +Titanium.Utils.base64encode(username+':'+password));
		 
		 // Send the request.
		 client.send(data);
		 
		 return client;
	}
	
		
	win.makeRequest('http://nooku-clean.loc/?option=com_articles','GET', function(response)
	{
		for(var i in response.items)
		{
			var item = response.items[i]
			table.appendRow ({
				title: item.data.title,
				desc: item.data.description,
				id: item.data.id,
				data: item.data
			});
		}
	})
	
	return self;
};

module.exports = MasterView;
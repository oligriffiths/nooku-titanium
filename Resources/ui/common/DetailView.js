function DetailView(parent, win) {
	var self = Ti.UI.createView();
	
	var title = Ti.UI.createTextField({
	  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	  color: '#336699',
	  top: 10,
	  left: 10,
	  right: 10,
	  width: Ti.App.SCREEN_WIDTH, 
	  height: 30
	});
	self.add(title)
	
	
	var desc = Ti.UI.createTextArea({
	  	borderWidth: 2,
	  	borderColor: '#bbb',
	  	borderRadius: 5,
	  	color: '#888',
	  	font: {fontSize:20, fontWeight:'bold'},
	  	keyboardType: Ti.UI.KEYBOARD_DEFAULT,
	  	returnKeyType: Ti.UI.RETURNKEY_DEFAULT,
	  	textAlign: 'left',
	  	top: 50,
	  	left: 10,
	  	right: 10,
	  	width: Ti.App.SCREEN_WIDTH, 
	  	height: 300,
	});
	self.add(desc);


	var saveButton = Titanium.UI.createButton({
		title: 'Save',
		systemButton: Ti.UI.iPhone.SystemButton.DONE,
		bottom: 10,
	});
	
	saveButton.addEventListener('click',function(e)
	{
   		var data = {
   			title: title.value,
   			description: desc.value,
   			published: 1
   		}
   		
   		win.makeRequest('http://nooku-clean.loc/?option=com_articles&view=article&id='+row.id, 'PUT', function(response, status, e){
   			
   			if(status == 205 || status == 204)
   			{
   				alert('Data saved')
   			}else{
   				alert('Resource was unable to be saved')
   			}	         
   		}, data)

	});
	
	self.add(saveButton);

	var row;
	self.addEventListener('itemSelected', function(e) {
		parent.title = e.title;
		title.value = e.title;		
		desc.value = e.desc;
		row = e;
		Ti.API.info(row)
	});
	
	return self;
};

module.exports = DetailView;

function ApplicationWindow() {
	//declare module dependencies
	var MasterView = require('ui/common/MasterView'),
		DetailView = require('ui/common/DetailView');

	//create object instance
	var self = Ti.UI.createWindow({
		backgroundColor:'#ffffff'
	});
	
	
	//create master view container
	var masterContainerWindow = Ti.UI.createWindow({
		title:'Articles'
	});
		
	//create detail view container
	var detailContainerWindow = Ti.UI.createWindow({
		title:'Article'
	});
	
	//construct UI
	var masterView = new MasterView(masterContainerWindow, self),
		detailView = new DetailView(detailContainerWindow, self);
		
	masterContainerWindow.add(masterView);
	detailContainerWindow.add(detailView);
	
	//create iOS specific NavGroup UI
	var navGroup = Ti.UI.iPhone.createNavigationGroup({
		window:masterContainerWindow
	});
	self.add(navGroup);
	
	//add behavior for master view
	masterView.addEventListener('itemSelected', function(e) {
		detailView.fireEvent('itemSelected',e);
		navGroup.open(detailContainerWindow);
	});
	
	return self;
};

module.exports = ApplicationWindow;
